/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejer3;
/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva  C.I.:8537754
 */
public class pila3 <E>
{int arr[];
    int top;
    
    pila3(){
        this.arr =new int[3];
        this.top =-1;
    }
    public boolean empty(){
        return (top==-1);
    }
    public void push(int  element){
        top++;
        if(top<arr.length){
            arr[top]=element;
        }else{
            int temp[]=new int[arr.length+2];
            for(int i=0;i<arr.length;i++){
                temp[i]=arr[i];
            }
            arr = temp;
            arr[top]=element;
        }
    }
    public int pop(){
        if(!empty()){
            int returntop =top;
            top--;
            int temp =arr[returntop];
            return temp;
        }else{
            System.out.println("la pila ya esta vacia ");
            return -1;
        }
    }
    public int peek(){
        return arr[top];
    }
}
