package ejer1;

/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva C.I.: 8537754
 */
import java.util.Scanner;

public class mainpila1 {

    public static void main(String[] args) {
        
        // Mostramos menu dede opciones
        System.out.println("que tipo de pila desea crear?: \n==================== \n1.- Cadenas");
        System.out.println("2.- Enteros");
        System.out.println("3.- Ambos");

        Scanner x = new Scanner(System.in);
        // Guardamos la opcion seleccionada
        int option = x.nextInt();
        System.out.println("ingrese el tamano de la pila: ");
        // Guardamos el tamano de la pila
        int size = x.nextInt();
        menutwo(size, option);

        //int continues=0;
        //while(continues)
    }

    /**
     * Menu con mas opciones para pilas
     *
     * @param size --> tamano de la pila
     * @param option --> Opcion seleccionada (1. agregar texto, 2, agregar
     * numeros, 3 ambos)
     */
    public static void menutwo(int size, int option) {
        Scanner z = new Scanner(System.in);
        /*System.out.println("1.- añadir elementos");
        System.out.println("2.- Eliminar un elemento");
        System.out.println("3.- Cuantos elementos existen y cuantos espacios libres quedan");
        System.out.println("4.- Ver el elemento máximo y mínimo");
        System.out.println("5.- Buscar elementos dentro de la pila");
        System.out.println("6.- Ordenar los elementos");
        System.out.println("7.- Sacar todos los elmentos");
        System.out.println("8.- Finalizar");*/

        // Definimos objeto global
        pilagen pt = new pilagen(size);
        menu m = new menu(pt);
        int continues  = 0;
        while(continues == 0){
            System.out.println("1.- añadir elementos");
            System.out.println("2.- Eliminar un elemento");
            System.out.println("3.- Cuantos elementos existen y cuantos espacios libres quedan");
            System.out.println("4.- Ver el elemento máximo y mínimo");
            System.out.println("5.- Buscar elementos dentro de la pila");
            System.out.println("6.- Ordenar los elementos");
            System.out.println("7.- Sacar todos los elmentos");
            System.out.println("8.- Finalizar");
            
            System.out.println("Que desea hacer?");
            int optionSelected = z.nextInt();
            switch (optionSelected) {
                case 1:
                    System.out.println("Valor a intr");
                    Object value = z.nextInt();
                    m.insert(value);
                    break;
                case 2:
                    System.out.println("sacano datos");
                    m.mostrar();
                    break;
                default:
                    System.out.println("----------");
                    break;
            }
            System.out.println("apra conti pre 0 ");
            continues = z.nextInt();
        }
        /*int optionTwo = z.nextInt();
        switch (optionTwo) {
            case 1:
                switch (option) {
                    case 1:
                        caseone(size, m);
                       //menutwo(size, option);
                        break;
                    case 2:
                        caseTwo(size);
                        menutwo(size, option);
                        break;
                    case 3:
                        caseThree(size);
                        menutwo(size, option);
                        break;
                    default:
                        System.out.println("dato no valido");
                        break;
                }

            case 2:
                System.out.println("el elemento se elimino: " + pt.pop());
                pt.pop();
                menutwo(size, option);
                break;
            case 3:
                System.out.println("los elementos que existen son: " + pt.size());

                System.out.println("los espacios libres son: " + pt.free());

                menutwo(size, option);
                break;
            case 4:
                menutwo(size, option);
                break;
            case 5:
                menutwo(size, option);
                break;
            case 6:
                //sortpila(p);
                menutwo(size, option);
                break;
            case 7:
                //System.out.println(pt.pop());
                //menutwo(size, option);
                m.mostrar();
                break;
            case 8:
                System.out.println("proceso finalizado");
                break;
            default:
                System.out.println("no existe opcion: " + optionTwo);
                break;
        }*/

    }

    /**
     * Caso 1 agregar datos cadenas
     *
     * @param size --> Tamano, de cuantos datos se dese agregar
     */
    public static void caseone(int size, menu m) {
        System.out.println("CASO 1: Agregar cadenas");
        Scanner y = new Scanner(System.in);
        String data = "";
        //pilagen pt = new pilagen(size);
for (int i = 1; i <= size; i++) {
            System.out.println("ingrese el valor " + i);
            data = y.nextLine();
            if (data.matches("[0-9]+")) {
                System.out.println("no se permiten caracteres numericos");
                break;
            }
            //pt.push(data);
            m.insert(data);
        }
        // return pt;
    }

    /**
     * Caso 2 agregar datos numericos
     *
     * @param size --> Tamano, de cuantos datos se dese agregar
     */
    public static void caseTwo(int size) {
        System.out.println("CASO 2: Agregar enteros");
        Scanner y = new Scanner(System.in);
        int data;
        pilagen pt = new pilagen(size);
        for (int i = 1; i <= size; i++) {
            System.out.println("ingrese el valor " + i);
            data = y.nextInt();
            pt.push(data);
        }
    }

    /**
     * Caso 3 agregar datos enteros y texto
     *
     * @param size --> Tamano, de cuantos datos se dese agregar
     */
    public static void caseThree(int size) {
        System.out.println("CASO 3: Agregar cadenas y enteros");
        Scanner y = new Scanner(System.in);
        String data;
        pilagen pt = new pilagen(size);
        for (int i = 1; i <= size; i++) {
            System.out.println("ingrese el valor " + i);
            data = y.nextLine();
            pt.push(data);
        }
    }

    /*public static void sortpila(pilagen p){
        pilagen tp = new pilagen(size);
        while(!p.empty()){
            int data = p.pop();
            while(!tp.empty() && tp.peek() > data){ 
                p.push(tp.pop());
            }
            tp.push(data);
        }
        return tp;*/
}
