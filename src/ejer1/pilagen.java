package ejer1;
/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva    C.I.: 8537754
 */
public class pilagen{
    Object arr[];
    int maxsize;
    int top;
    
    public pilagen(int n){
        this.maxsize = n;
        this.arr = new Object[maxsize];
        this.top = 0;
    }
    public boolean empty(){
        return top==0;
    }
    public void push(Object n){
        if(top<maxsize){
            arr[top] = n;
            top++;
        }
    }
    public Object pop(){
        Object temp=-1;
        if(top>0){
            temp=arr[top-1];
            top--;
        }
        return temp;
    }
    public Object peek(){
        if(top>0){
            return arr[top-1];
        }else{
            return null;
        }      
    }
    public Object size(){
        return arr[top];
    }
    public int free(){
        return maxsize-top;
    }
}


