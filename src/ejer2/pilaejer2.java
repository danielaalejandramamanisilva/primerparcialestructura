package ejer2;

/**
 *
 * @author Martin Orsi Mamani
 */
public class pilaejer2 {
    char arr[];
    int top;
    
    pilaejer2(){
        this.arr=new char[10];
        this.top=-1;
    }
    public boolean empty(){
        return(top==-1);
    }
    public void push(char elem){
        top++;
        if(top<arr.length){
            arr[top]=elem;
        }else{
            char tempo[]= new char[arr.length+5];
            for(int i = 0; i < arr.length; i++){
                tempo[i]=arr[i];
            }
            arr=tempo;
            arr[top]=elem;
        }
    }
    public char peek(){
        return arr[top];
    }
    public char pop(){
        if(!empty()){
            int tempotop=top;
            top--;
            char returntop = arr[tempotop];
            return returntop;
        }else{
            System.out.println("mi pila esta vacia ");
            return '-';
        }
    }
}
